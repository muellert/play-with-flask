import pytest

from contacts.app import app, db
from contacts.models import Contact


@pytest.fixture
def nclient():
    app.config['TESTING'] = True
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    with app.test_client() as client:
        with app.app_context():
            db.init_app(app)
            db.create_all()
        return client


@pytest.fixture(scope="function")
def client(count=3):
    app.config['TESTING'] = True
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    with app.test_client() as client:
        with app.app_context():
            db.init_app(app)
            db.create_all()
            for i in range(count):
                db.session.add(Contact(first=f"first_{i}",
                                       last=f"last_{i}",
                                       username=f"u_{i}"))
                try:
                    db.session.commit()
                except:
                    pass        # we already have this record
            db.session.commit()
        return client
