
import json
# import pytest


def test_contact_list(client):
    """list all the contacts in the database"""
    rv = client.get('/contacts')
    # import pdb; pdb.set_trace()
    assert rv.status_code == 200
    resp = json.loads([l for l in rv.response][0])
    assert isinstance(resp, type([]))


def test_contact_find_one(client):
    """find 'u_1' in the database"""
    rv = client.get('/contacts/u_1')
    assert rv.status_code == 200


def test_contact_find_none(client):
    """search nonexistent user in the database"""
    rv = client.get('/contacts/xxx')
    assert rv.status_code == 404


def test_contact_add_bob(client):
    """add 'bob' to the database"""
    rv = client.post('/contacts',
                     json={'first': 'bobby',
                           'last': 'smith',
                           'username': 'bob'})
    assert rv.status_code == 200


def test_contact_add_duplicate_user(client):
    """add 'bob' to the database twice"""
    rv = client.post('/contacts',
                     json={'first': 'bobby',
                           'last': 'smith',
                           'username': 'bob'})
    rv = client.post('/contacts',
                     json={'first': 'xyz',
                           'last': 'xyzb',
                           'username': 'bob'})
    assert rv.status_code == 405


def test_contact_edit_contact(client):
    """edit 'u_2' to the database"""
    rv = client.put('/contacts/u_2',
                    json={'first': 'jack',
                          'last': 'jones'})
    assert rv.status_code == 200


def test_delete_user(client):
    client.delete('/contacts/u_1')
    rv = client.get('/contacts/u_1')
    assert rv.status_code == 404


def test_delete_user_twice(client):
    client.delete('/contacts/u_1')
    rv = client.delete('/contacts/u_1')
    assert rv.status_code == 405


def test_contact_add_james_with_email(client):
    """add 'james' to the database, with email"""
    rv = client.post('/contacts',
                     json={'first': 'james',
                           'last': 'brown',
                           'username': 'jbrown',
                           'emails': ['a@example.com', 'b@example.com']})
    assert rv.status_code == 200


def test_contact_can_we_read_emailaddrs(client):
    """can we read the email addresses?"""
    rv = client.post('/contacts',
                     json={'first': 'james',
                           'last': 'brown',
                           'username': 'jbrown',
                           'emails': ['a@example.com', 'b@example.com']})
    assert rv.status_code == 200
    rv = client.get('/contacts/jbrown')
    assert 'a@example.com' in json.loads(rv.get_json())['emailaddresses']
