import json

import datetime

from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()


class Contact(db.Model):
    __tablename__ = 'contact'
    id = db.Column(db.Integer, primary_key=True)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    first = db.Column(db.String(50), nullable=False)
    last = db.Column(db.String(50), nullable=False)
    username = db.Column(db.String(50),
                         nullable=False,
                         unique=True)
    emailaddresses = db.relationship('EmailAddress',
                                     backref='contact',
                                     lazy=False)

    def to_json(self):
        d = {'id': self.id, 'first': self.first,
             'last': self.last, 'username': self.username}
        d['emailaddresses'] = [e.emailaddress for e in self.emailaddresses]
        return json.dumps(d)

    @classmethod
    def from_json(cls, jsd):
        newcontact = Contact()
        for k in ('first', 'last', 'username'):
            newcontact.__setattr__(k, jsd[k])
        if 'id' in jsd:
            newcontact.id = jsd['id']
        return newcontact


def list():
    return Contact.query.all()


def find(contact):
    """contact by name"""
    contact = Contact.query.filter_by(username=contact).first()
    return contact


def findbytime(age):
    """contact by name"""
    backthen = datetime.datetime.utcnow() - age
    cl = Contact.query.filter_by(created<=backthen).all()
    return cl


def delete(contact):
    u = find(contact)
    if u is not None:
        db.session.delete(u)
        db.session.commit()
    return u


def update(contact, data):
    u = find(contact)
    # import pdb; pdb.set_trace()
    if u is None:
        raise ContactNotFoundError
    for a in 'first', 'last', 'username':
        if a in data:
            u.__setattr__(a, data[a])
    db.session.add(u)
    db.session.commit()
    return u


def create(jsd):
    c = Contact.from_json(jsd)
    if 'emails' in jsd:
        for email in jsd['emails']:
            print("create: adding email {}".format(email))
            ea = EmailAddress(emailaddress=email)
            print("create: ea = {}".format(ea))
            c.emailaddresses.append(ea)
    db.session.add(c)
    db.session.commit()
    return c


class ContactNotFoundError(ValueError):
    pass


# Email:

class EmailAddress(db.Model):
    __tablename__ = 'emailaddresses'
    id = db.Column(db.Integer, primary_key=True)
    contact_id = db.Column(db.Integer, db.ForeignKey('contact.id'),
                           nullable=False)
    emailaddress = db.Column(db.String(200), nullable=False)
