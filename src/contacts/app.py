import string
import random


from flask import Flask, jsonify, request

from celery import Celery


from .models import db
from .models import list, find, update, delete, create, findbytime


# Main Flask application:

app = Flask(__name__)
app.config.from_pyfile("config.py", silent=True)
db.init_app(app)
celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)


# Celery tasks:

@celery.task(bind=True)
def add_random_contact(self):
    first = gen_random_string(6)
    last = gen_random_string(6)
    uname = gen_random_string(6)
    c = {'first': first, 'last': last, 'username': uname}
    create(c)
    self.apply_async(countdown=15)


@celery.task(bind=True)
def delete_old_contacts(self):
    cl = findbytime(60)
    for c in cl:
        delete(c.username)
    self.apply_async(countdown=60)


add_random_contact.apply_async(countdown=15)
delete_old_contacts.apply_async(countdown=60)


@app.route('/contacts', methods=['GET'])
def list_contacts():
    cl = [c.to_json() for c in list()]
    return jsonify(cl)


@app.route('/contacts', methods=['POST'])
def create_contact():
    data = request.get_json()
    u = find(data['username'])
    if u is not None:
        response = jsonify({'error': 'username {} already exists!'.format(u)})
        response.status_code = 405
        return response
    return create(data).to_json()


@app.route('/contacts/<name>', methods=['GET'])
def find_contacts(name):
    c = find(name)
    if c is None:
        response = jsonify(None)
        response.status_code = 404
        return response
    return jsonify(c.to_json())


@app.route('/contacts/<name>', methods=['PUT'])
def edit_contact(name):
    data = request.get_json()
    return jsonify(update(name, data).to_json())


@app.route('/contacts/<name>', methods=['DELETE'])
def delete_contact(name):
    result = delete(name)
    if result is not None:
        result = jsonify(result.to_json())
    else:
        result = jsonify(None)
        result.status_code = 405
    return result


# Helper functions:

def gen_random_string(l):
    return ''.join(random.choice(string.ascii_lowercase) for i in range(l))
