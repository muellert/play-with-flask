from setuptools import setup, find_packages


with open('requirements-prod.txt') as req:
    install_requires = [line for line in req.readlines()
                        if not line.startswith('#')]

with open('requirements-develop.txt') as req:
    test_requires = [line for line in req.readlines()
                     if not line.startswith('#')]


setup(
    name="contacts",
    version="0.1",
    package_dir={'': 'src'},
    packages=find_packages('contacts', exclude=['tests']),
    python_requires=">=3.7",
    install_requires=install_requires,
    tests_require=test_requires,
    include_package_data=True,
    author='Toni Mueller',
    author_email='work@tonimueller.org',
    platforms=['linux'],
    zip_safe=False,
)
